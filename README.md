Chess Game Project
==================

Copyright 2014 by Ethan Ruffing

Overview
--------
This is a chess game, designed to be an interactive game for two human
players. A chess board is drawn on the screen, and the two players then
manipulate the pieces on that board according to the basic rules of
chess until the game has been won.

This project was performed for Computer Science II (CIS 163), Section 4,
at Grand Valley State University, instructed by Dr. Christian Trefftz.
The project was submitted on March 20, 2014.

UML Diagram
-----------
<img src="ChessProject.svg" />

Changelog
---------
### 1.2.2 ###
* Modified project git setup

### 1.2.1 ###
* Added UML Diagrams
* Added README.md file

### 1.2.0 ###
First portfolio-ready release
* Fixed large portions of end-of-game recognition
* Reformatted "provided" files for printing
* Reformatted ChessPanel.java and ChessModel.java
* Added version tags
* Fixed some JavaDoc tags 

### 1.1.1 ###
* Added a `.gitignore` file

### 1.1.0 ###
* Merged with sources from original school project found on Linux
  computer

### 1.0.0 ###
* Sources from original school project found on Windows computer