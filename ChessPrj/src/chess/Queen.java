/***********************************************************************
 * A queen class. This class extends the <code>ChessPiece</code> class,
 * implementing actions specific to the queen piece.
 * 
 * @author Ethan Ruffing
 * @version 1.2.2
 **********************************************************************/
package chess;

import W14Project3.IChessPiece;
import W14Project3.Move;
import W14Project3.Player;

public class Queen extends ChessPiece {

	/*******************************************************************
	 * Default constructor. Creates a new queen with the specified
	 * player as its owner.
	 * 
	 * @param player The player the bishop will play for
	 ******************************************************************/
	public Queen(Player player) {
		super(player);
	}

	/*******************************************************************
	 * A string representation of the type of chess piece.
	 * 
	 * @return "Queen"
	 ******************************************************************/
	public String type() {
		return "Queen";
	}

	/*******************************************************************
	 * Determines whether a given move is valid for a Queen. Allows only
	 * diagonal or straight moves of any length and does not allow jumps
	 * over other pieces, with additional limits set by
	 * <code>isValidMove</code> in <code>ChessPiece</code>.
	 * 
	 * @param move The move to test
	 * @param board The board the move would be performed on
	 ******************************************************************/
	public boolean isValidMove(Move move, IChessPiece[][] board) {
		boolean valid = super.isValidMove(move, board);

		// Diagonal moves allowed, but not across other pieces, and only
		// in 45 degree diagonals
		if (move.toRow != move.fromRow
				&& move.toColumn != move.fromColumn) {
			// Must be direct diagonal
			if (Math.abs(move.toRow - move.fromRow) != Math
					.abs(move.toColumn - move.fromColumn)) {
				valid = false;
			} else {
				// Test diagonal lines for pieces in the way
				if (move.toRow > move.fromRow
						&& move.toColumn > move.fromColumn) {
					for (int i = move.fromRow + 1, j =
							move.fromColumn + 1; i < move.toRow
							&& j < move.toColumn; i++, j++) {
						if (board[i][j] != null) {
							valid = false;
						}
					}
				} else if (move.toRow > move.fromRow
						&& move.toColumn < move.fromColumn) {
					for (int i = move.fromRow + 1, j =
							move.fromColumn - 1; i < move.toRow
							&& j > move.toColumn; i++, j--) {
						if (board[i][j] != null) {
							valid = false;
						}
					}
				} else if (move.toRow < move.fromRow
						&& move.toColumn > move.fromColumn) {
					for (int i = move.fromRow - 1, j =
							move.fromColumn + 1; i > move.toRow
							&& j < move.toColumn; i--, j++) {
						if (board[i][j] != null) {
							valid = false;
						}
					}
				} else {
					for (int i = move.fromRow - 1, j =
							move.fromColumn - 1; i > move.toRow
							&& j > move.toColumn; i--, j--) {
						if (board[i][j] != null) {
							valid = false;
						}
					}
				}
			}
		}

		// Vertical moves allowed, but not across other pieces
		if (move.toRow != move.fromRow
				&& move.toColumn == move.fromColumn) {
			for (int i = move.fromRow + 1; i < move.toRow; i++) {
				if (board[i][move.fromColumn] != null) {
					valid = false;
				}
			}
			for (int i = move.fromRow - 1; i > move.toRow; i--) {
				if (board[i][move.fromColumn] != null) {
					valid = false;
				}
			}
		}

		// Horizontal moves allowed, but not across other pieces
		if (move.toRow == move.fromRow
				&& move.toColumn != move.fromColumn) {
			for (int i = move.fromColumn + 1; i < move.toColumn; i++) {
				if (board[move.fromRow][i] != null) {
					valid = false;
				}
			}
			for (int i = move.fromColumn - 1; i > move.toColumn; i--) {
				if (board[move.fromRow][i] != null) {
					valid = false;
				}
			}
		}

		return valid;
	}
}
