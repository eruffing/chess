/***********************************************************************
 * A graphical front end of the <code>ChessModel</code>.
 * 
 * @author Ethan Ruffing
 * @version 1.2.2
 **********************************************************************/
package chess;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;
import javax.swing.event.MenuDragMouseEvent;
import javax.swing.event.MenuDragMouseListener;

import W14Project3.Move;
import W14Project3.Player;

public class ChessPanel extends JPanel {

	/** An array of buttons to represent the game board. */
	private JButton[][] board;

	/** A <code>ChessModel</code> to handle the game. */
	private ChessModel model;

	/** A menu bar for the top of the window. */
	private JMenuBar menuBar;

	/** The "file" menu. */
	private JMenu fileMenu;

	/** A "game" menu for game controls. */
	private JMenu gameMenu;

	/** The "help" menu. */
	private JMenu helpMenu;

	/** A quit option for the file menu. */
	private JMenuItem quitItem;

	/** A game menu option to reset the game. */
	private JMenuItem resetItem;

	/** An about option for the help menu. */
	private JMenuItem aboutItem;

	/** A label to show what pieces white has captured. */
	private JLabel whiteTakenLabel;

	/** A label to show what pieces black has captured. */
	private JLabel blackTakenLabel;

	/** A separator bar to place between the two "captured" labels. */
	private JSeparator takenSeparator;

	/** A label to show which player's turn it is. */
	private JLabel playerLabel;

	/** A separator for the <code>playerLabel</code>. */
	private JSeparator playerSeparator;

	/** A panel to contain the score and control buttons. */
	private JPanel controlPanel;

	/** A panel to contain the game board. */
	private JPanel boardPanel;

	/** A boolean of whether the first click for a move has been made.*/
	private boolean firstClicked;

	/** The starting row of a move being made. */
	private int r0;

	/** The starting column of a move being made. */
	private int c0;

	/**
	 * A <code>BorderLayout</code> to manage the relative layout of the
	 * control buttons and game board.
	 */
	private BorderLayout panelLayout;

	/**
	 * A <code>GridLayout</code> to manage the layout of the
	 * <code>boardPanel</code>.
	 */
	private GridLayout boardLayout;

	/*******************************************************************
	 * Default constructor. Creates a panel and menu bar for a standard
	 * eight-by-eight chess board.
	 ******************************************************************/
	public ChessPanel() {
		// TODO Complete this

		// Initialize the game and the board
		model = new ChessModel();
		firstClicked = false;
		r0 = -1;
		c0 = -1;
		board = new JButton[model.numRows()][model.numColumns()];

		// Create layout for the panel
		panelLayout = new BorderLayout();

		// Initialize sub-panels
		controlPanel = new JPanel();
		boardPanel = new JPanel();

		// Initialize layouts for sub-panels
		boardLayout = new GridLayout(board.length, board[0].length);

		// Construct menus and add appropriate listeners
		menuBar = new JMenuBar();
		fileMenu = new JMenu("File");
		gameMenu = new JMenu("Game");
		helpMenu = new JMenu("Help");
		quitItem = new JMenuItem("Exit");
		quitItem.addMenuDragMouseListener(new MenuListener());
		quitItem.addMouseListener(new MenuListener());
		resetItem = new JMenuItem("Reset Game");
		resetItem.addMenuDragMouseListener(new MenuListener());
		resetItem.addMouseListener(new MenuListener());
		aboutItem = new JMenuItem("About");
		aboutItem.addMenuDragMouseListener(new MenuListener());
		aboutItem.addMouseListener(new MenuListener());
		fileMenu.add(quitItem);
		gameMenu.add(resetItem);
		helpMenu.add(aboutItem);
		menuBar.add(fileMenu);
		menuBar.add(gameMenu);
		menuBar.add(helpMenu);

		// Initialize labels for taken pieces
		whiteTakenLabel = new JLabel("Captured by White: 0");
		blackTakenLabel = new JLabel("Captured by Black: 0");
		takenSeparator = new JSeparator(SwingConstants.VERTICAL);

		// Initialize label for current player
		playerLabel = new JLabel("Current Player: ");
		playerSeparator = new JSeparator(SwingConstants.VERTICAL);

		// Add labels to controlPanel
		controlPanel.add(whiteTakenLabel);
		controlPanel.add(takenSeparator);
		controlPanel.add(blackTakenLabel);
		controlPanel.add(playerSeparator);
		controlPanel.add(playerLabel);

		// Initialize the buttons within the board and add it to board
		// layout and panel
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[0].length; col++) {
				board[row][col] = new JButton("");
				board[row][col].addActionListener(new ButtonListener());
				boardLayout.addLayoutComponent("Button " + row + ", "
						+ col, board[row][col]);
				boardPanel.add(board[row][col]);
			}
		}

		// Set board layout
		boardPanel.setLayout(boardLayout);

		// Add board contents
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[0].length; col++) {
				boardPanel.add(board[row][col]);
			}
		}

		// Add items to game panel in appropriate layout
		panelLayout
				.addLayoutComponent(controlPanel, BorderLayout.NORTH);
		panelLayout.addLayoutComponent(boardPanel, BorderLayout.CENTER);
		this.setLayout(panelLayout);
		this.add(controlPanel);
		this.add(boardPanel);

		displayBoard();
	}

	/*******************************************************************
	 * Updates the displayed board to represent the current status of
	 * the game.
	 ******************************************************************/
	private void displayBoard() {
		// Set board background
		resetBackground();

		// Set gamepiece icons
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[0].length; col++) {
				if (model.pieceAt(row, col) == null) {
					// Set empty spots
					board[row][col].setText("");
				} else if (model.pieceAt(row, col).player() 
						== Player.WHITE) {
					// Set white pieces
					String type = model.pieceAt(row, col).type();
					if (type.equals("Pawn")) {
						board[row][col].setText("\u2659");
					} else if (type.equals("Rook")) {
						board[row][col].setText("\u2656");
					} else if (type.equals("Knight")) {
						board[row][col].setText("\u2658");
					} else if (type.equals("Bishop")) {
						board[row][col].setText("\u2657");
					} else if (type.equals("Queen")) {
						board[row][col].setText("\u2655");
					} else if (type.equals("King")) {
						board[row][col].setText("\u2654");
					}
				} else if (model.pieceAt(row, col).player() 
						== Player.BLACK) {
					// Set black pieces
					String type = model.pieceAt(row, col).type();
					if (type.equals("Pawn")) {
						board[row][col].setText("\u265F");
					} else if (type.equals("Rook")) {
						board[row][col].setText("\u265C");
					} else if (type.equals("Knight")) {
						board[row][col].setText("\u265E");
					} else if (type.equals("Bishop")) {
						board[row][col].setText("\u265D");
					} else if (type.equals("Queen")) {
						board[row][col].setText("\u265B");
					} else if (type.equals("King")) {
						board[row][col].setText("\u265A");
					}
				} else {
					// Set empty spots
					board[row][col].setText("");
				}

				// Set square size
				board[row][col].setFont(board[row][col].getFont()
						.deriveFont((float) 36));
			}
		}

		// Update captured labels
		whiteTakenLabel.setText("Captured by White: "
				+ model.getCapturedByWhite());
		blackTakenLabel.setText("Captured by Black: "
				+ model.getCapturedByBlack());
		playerLabel.setText("Current Player: "
				+ model.currentPlayer().toString());

		// After performing a move, test the current game
		// state, and alert the player(s) accordingly
		if (model.isComplete()) {
			if (model.inCheck(Player.WHITE)
					|| (model.getWinner() != null 
					&& model.getWinner() == Player.BLACK)) {
				JOptionPane.showMessageDialog(controlPanel,
						"Game over. White is in check mate "
								+ "or has been captured.\n"
								+ "Black wins!");
			} else if (model.inCheck(Player.BLACK)
					|| (model.getWinner() != null
					&& model.getWinner() == Player.WHITE)) {
				JOptionPane.showMessageDialog(controlPanel,
						"Game over. Black is in check mate "
								+ "or has been captured.\n"
								+ "White wins!");
			} else {
				JOptionPane.showMessageDialog(controlPanel,
						"Game over. Game is in stalemate.");
			}
		} else if (model.inCheck(Player.WHITE)) {
			JOptionPane.showMessageDialog(controlPanel,
					"White is in check!");
		} else if (model.inCheck(Player.BLACK)) {
			JOptionPane.showMessageDialog(controlPanel,
					"Black is in check!");
		}
	}

	/*******************************************************************
	 * Resets the board backgrounds to normal pattern.
	 ******************************************************************/
	private void resetBackground() {
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[0].length; col++) {
				if ((row + col) % 2 != 0) {
					board[row][col].setBackground(Color.WHITE);
				} else {
					board[row][col].setBackground(Color.LIGHT_GRAY);
				}
			}
		}
	}

	/*******************************************************************
	 * Service method to access the <code>JMenuBar</code> constructed
	 * for the game.
	 * 
	 * @return The menu bar constructed for the game
	 ******************************************************************/
	public JMenuBar getMenuBar() {
		return menuBar;
	}

	/*******************************************************************
	 * Displays a dialog with information about the program.
	 ******************************************************************/
	private void showAboutDialog() {
		// Construct string with program information
		String aboutString = "Chess game written by ";
		aboutString += "Ethan Ruffing\nfor CIS 163-04 at Grand ";
		aboutString += "Valley State University.\n";
		aboutString += "Date: March 20, 2014.\n";
		aboutString += "Gamepiece icons were obtained as part of the\n";
		aboutString += "Unicode character set.";

		// Display about dialog
		JOptionPane.showMessageDialog(controlPanel, aboutString,
				"About", JOptionPane.INFORMATION_MESSAGE);
	}

	/*******************************************************************
	 * Confirms that the user wants to reset the game, then does so.
	 ******************************************************************/
	private void confirmReset() {
		int reset =
				JOptionPane.showConfirmDialog(controlPanel,
						"Are you sure you want to reset?", "Reset?",
						JOptionPane.YES_NO_OPTION);
		if (reset == JOptionPane.YES_OPTION) {
			// Re-initialize the model, and display new game
			model = new ChessModel();
			displayBoard();
		}
	}

	/*******************************************************************
	 * Confirms that the user wants to quit the game, then exits.
	 ******************************************************************/
	private void confirmQuit() {
		int quit =
				JOptionPane.showConfirmDialog(controlPanel,
						"Are you sure you want to quit", "Quit?",
						JOptionPane.YES_NO_OPTION);
		if (quit == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}

	/*******************************************************************
	 * A listener for all <code>JButton</code>s in the
	 * <code>boardPanel</code>.
	 * 
	 * @author Ethan Ruffing
	 ******************************************************************/
	private class ButtonListener implements ActionListener {

		/***************************************************************
		 * Method called when a button on the game board is clicked.
		 * Handles responses to game play.
		 **************************************************************/
		public void actionPerformed(ActionEvent e) {
			// If this is the first click on the game piece, prepare to
			// move it and highlight all possible moves for the selected
			// piece
			if (!firstClicked) {
				r0 = -1;
				c0 = -1;
				for (int row = 0; row < board.length; row++) {
					for (int col = 0; col < board[0].length; col++) {
						if (e.getSource() == board[row][col]) {
							r0 = row;
							c0 = col;
						}
					}
				}
				if (model.pieceAt(r0, c0) != null) {
					if (model.pieceAt(r0, c0).player() == model
							.currentPlayer()) {
						for (int row = 0; row < board.length; row++) {
							for (int col = 0; col < board[0].length;
									col++) {
								if (model.isValidMove(new Move(r0, c0,
										row, col))) {
									board[row][col]
											.setBackground(Color.PINK);
								}
							}
						}
						firstClicked = true;
					}
				}
			} else {
				// If this is a click after the first click, test
				// whether a move can be made to the selected square by
				// the previously selected piece
				int r1 = -1;
				int c1 = -1;
				for (int row = 0; row < board.length; row++) {
					for (int col = 0; col < board[0].length; col++) {
						if (e.getSource() == board[row][col]) {
							r1 = row;
							c1 = col;
						}
					}
				}

				// Create the move
				Move move = new Move(r0, c0, r1, c1);

				// If the move is valid perform it
				if (model.isValidMove(move)) {
					model.move(move);

				}

				// After a turn, update the game board
				displayBoard();
				firstClicked = false;
			}

		}
	}

	/*******************************************************************
	 * A listener for menu items. Initiates the appropriate response for
	 * each menu item.
	 * 
	 * @author Ethan Ruffing
	 ******************************************************************/
	private class MenuListener implements MenuDragMouseListener,
			MouseListener {

		/***************************************************************
		 * Called when the mouse is dragged to an option in the menu
		 * bar.
		 **************************************************************/
		@Override
		public void menuDragMouseReleased(MenuDragMouseEvent e) {
			if (e.getSource() == quitItem) {
				confirmQuit();
			}
			if (e.getSource() == resetItem) {
				confirmReset();
			}
			if (e.getSource() == aboutItem) {
				showAboutDialog();
			}
		}

		/***************************************************************
		 * Called when the mouse clicks on and releases an item in the
		 * menu bar.
		 **************************************************************/
		public void mouseReleased(MouseEvent e) {
			if (e.getSource() == quitItem) {
				confirmQuit();
			}
			if (e.getSource() == resetItem) {
				confirmReset();
			}
			if (e.getSource() == aboutItem) {
				showAboutDialog();
			}
		}

		// Unused methods to complete implementation of
		// MenuDragMouseListener
		public void menuDragMouseDragged(MenuDragMouseEvent e) {
		}

		public void menuDragMouseEntered(MenuDragMouseEvent e) {
		}

		public void menuDragMouseExited(MenuDragMouseEvent e) {
		}

		// Unused methods to complete implementation of MouseListener
		public void mouseEntered(MouseEvent arg0) {
		}

		public void mouseExited(MouseEvent arg0) {
		}

		public void mousePressed(MouseEvent arg0) {
		}

		public void mouseClicked(MouseEvent e) {
		}
	}

}
