/***********************************************************************
 * A king class. This class extends the <code>ChessPiece</code> class,
 * implementing actions specific to the king piece.
 * 
 * @author Ethan Ruffing
 * @version 1.2.2
 **********************************************************************/
package chess;

import W14Project3.IChessPiece;
import W14Project3.Move;
import W14Project3.Player;

public class King extends ChessPiece {

	/*******************************************************************
	 * Default constructor. Creates a new king with the specified player
	 * as its owner.
	 * 
	 * @param player The player the bishop will play for
	 ******************************************************************/
	public King(Player player) {
		super(player);
	}

	/*******************************************************************
	 * A string representation of the type of chess piece.
	 * 
	 * @return "King"
	 ******************************************************************/
	public String type() {
		return "King";
	}

	/*******************************************************************
	 * Determines whether a given move is valid for a king. Allows moves
	 * of one square in any direction, with additional limits set by
	 * <code>isValidMove</code> in <code>ChessPiece</code>. Also does
	 * not allow the king to put himself in jeopardy.
	 * 
	 * @param move The move to test
	 * @param board The board the move would be performed on
	 ******************************************************************/
	public boolean isValidMove(Move move, IChessPiece[][] board) {
		boolean valid = super.isValidMove(move, board);

		// Only one move in any direction
		if (move.toRow - move.fromRow < -1
				|| move.toRow - move.fromRow > 1) {
			valid = false;
		}
		if (move.toColumn - move.fromColumn < -1
				|| move.toColumn - move.fromColumn > 1) {
			valid = false;
		}

		// Simulate move and test whether it would put king in check
		if (valid) {
			// Determine if any pieces are able to attack the king
			for (int row = 0; row < board.length; row++) {
				for (int col = 0; col < board[0].length; col++) {
					if (board[row][col] != null) {
						if (board[row][col].player() != this.player()) {
							if (board[row][col].isValidMove(
									new Move(row, col, move.toRow,
											move.toColumn), board)) {
								valid = false;
							}
						}
					}
				}
			}
		}

		return valid;
	}

}
