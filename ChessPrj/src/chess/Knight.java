/***********************************************************************
 * A knight class. This class extends the <code>ChessPiece</code> class,
 * implementing actions specific to the knight piece.
 * 
 * @author Ethan Ruffing
 * @version 1.2.2
 **********************************************************************/
package chess;

import W14Project3.IChessPiece;
import W14Project3.Move;
import W14Project3.Player;

public class Knight extends ChessPiece {
	
	/*******************************************************************
	 * Default constructor. Creates a new knight with the specified
	 * player as its owner.
	 * 
	 * @param player The player the bishop will play for
	 ******************************************************************/
	public Knight(Player player) {
		super(player);
	}

	/*******************************************************************
	 * A string representation of the type of chess piece.
	 * 
	 * @return "Knight"
	 ******************************************************************/
	public String type() {
		return "Knight";
	}

	/*******************************************************************
	 * Determines whether a given move is valid for a knight. Only
	 * allows moves of two squares in one direction, and one square in
	 * the other direction, with additional limits set by
	 * <code>isValidMove</code> in <code>ChessPiece</code>.
	 * 
	 * @param move The move to test
	 * @param board The board the move would be performed on
	 ******************************************************************/
	public boolean isValidMove(Move move, IChessPiece[][] board) {
		boolean valid = super.isValidMove(move, board);

		// Move must be two rows, three columns, or two columns, three
		// rows
		int rowsMoved = Math.abs(move.toRow - move.fromRow);
		int colsMoved = Math.abs(move.toColumn - move.fromColumn);
		if (!((rowsMoved == 1 && colsMoved == 2) || (rowsMoved == 2 &&
													colsMoved == 1))) {
			valid = false;
		}

		return valid;
	}
}
