/***********************************************************************
 * This class contains methods to test the implementation of each of the
 * <code>ChessPiece</code> derived classes (<code>Bishop</code>,
 * <code>King</code>, <code>Knight</code>, <code>Pawn</code>,
 * <code>Queen</code>, and <code>Rook</code>). Also tests the
 * <code>player</code> method of the base class <code>ChessPiece</code>.
 * 
 * @author Ethan Ruffing
 * @version 1.2.2
 **********************************************************************/
package chess;

import static org.junit.Assert.*;

import org.junit.Test;

import W14Project3.Player;

public class TestChessPieces {

	/*******************************************************************
	 * Test that the <code>type</code> method in <code>Bishop</code>
	 * returns the correct string.
	 ******************************************************************/
	@Test
	public void testBishopType() {
		Bishop b = new Bishop(Player.WHITE);
		assertTrue("Expected \"Bishop\", got \"" + b.type() + "\"", b
				.type().equals("Bishop"));
	}

	/*******************************************************************
	 * Test that the <code>type</code> method in <code>King</code>
	 * returns the correct string.
	 ******************************************************************/
	@Test
	public void testKingType() {
		King k = new King(Player.WHITE);
		assertTrue("Expected \"King\", got \"" + k.type() + "\"", k
				.type().equals("King"));
	}

	/*******************************************************************
	 * Test that the <code>type</code> method in <code>Knight</code>
	 * returns the correct string.
	 ******************************************************************/
	@Test
	public void testKnightType() {
		Knight kn = new Knight(Player.WHITE);
		assertTrue("Expected \"Knight\", got \"" + kn.type() + "\"", kn
				.type().equals("Knight"));
	}

	/*******************************************************************
	 * Test that the <code>type</code> method in <code>Pawn</code>
	 * returns the correct string.
	 ******************************************************************/
	@Test
	public void testPawnType() {
		Pawn p = new Pawn(Player.WHITE);
		assertTrue("Expected \"Pawn\", got \"" + p.type() + "\"", p
				.type().equals("Pawn"));
	}

	/*******************************************************************
	 * Test that the <code>type</code> method in <code>Queen</code>
	 * returns the correct string.
	 ******************************************************************/
	@Test
	public void testQueenType() {
		Queen q = new Queen(Player.WHITE);
		assertTrue("Expected \"Queen\", got \"" + q.type() + "\"", q
				.type().equals("Queen"));
	}

	/*******************************************************************
	 * Test that the <code>type</code> method in <code>Rook</code>
	 * returns the correct string.
	 ******************************************************************/
	@Test
	public void testRookType() {
		Rook r = new Rook(Player.WHITE);
		assertTrue("Expected \"Rook\", got \"" + r.type() + "\"", r
				.type().equals("Rook"));
	}

	/*******************************************************************
	 * Tests that the <code>player</code> method in the
	 * <code>ChessPiece</code> base class returns the correct player
	 * (the piece's owner). Uses the <code>Pawn</code> class to perform
	 * the test. Note that <code>Pawn</code> does not override
	 * <code>player</code>, so this test does in fact test the method in
	 * the base class.
	 ******************************************************************/
	@Test
	public void testPlayer() {
		Pawn p = new Pawn(Player.WHITE);
		assertEquals("Expected WHITE, but got " + p.player(),
				Player.WHITE, p.player());

		p = new Pawn(Player.BLACK);
		assertEquals("Expected BLACK, but got " + p.player(),
				Player.BLACK, p.player());
	}

}
