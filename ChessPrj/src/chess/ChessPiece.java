/***********************************************************************
 * A model for a chess piece.
 * 
 * @author Ethan Ruffing
 * @version 1.2.2
 **********************************************************************/

package chess;

import W14Project3.IChessPiece;
import W14Project3.Move;
import W14Project3.Player;

public abstract class ChessPiece implements IChessPiece {

	/** The player who owns this chess piece. */
	private Player owner;

	/*******************************************************************
	 * Default constructor. Creates a new chess piece with the specified
	 * owner.
	 * 
	 * @param player The player who will be the owner of this piece
	 ******************************************************************/
	protected ChessPiece(Player player) {
		this.owner = player;
	}

	/*******************************************************************
	 * Returns a string representation of the type of chess piece, i.e.
	 * "Pawn", "Rook", "King", "Queen", etc.
	 ******************************************************************/
	public abstract String type();

	/*******************************************************************
	 * Service method to access the player who owns this chess piece.
	 ******************************************************************/
	public Player player() {
		return owner;
	}

	/*******************************************************************
	 * Determines whether a given move is valid for this piece.
	 * 
	 * @param move The movement to be tested
	 * @param board The board on which this move is to be performed
	 ******************************************************************/
	public boolean isValidMove(Move move, IChessPiece[][] board) {
		boolean valid = true;

		if (move.fromRow < 0 || move.fromColumn < 0 || move.toRow < 0
				|| move.toColumn < 0) {
			return false;
		}

		if (move.fromColumn == move.toColumn
				&& move.fromRow == move.toRow) {
			valid = false;
		}

		if (!(board[move.fromRow][move.fromColumn].type().equals(this
				.type()))
				&& (board[move.fromRow][move.fromColumn].player() == 
														this.owner)) {
			valid = false;
		}

		if (board[move.toRow][move.toColumn] != null) {
			if (board[move.toRow][move.toColumn].player() == 
														this.owner) {
				valid = false;
			}
		}

		return valid;
	}
}
