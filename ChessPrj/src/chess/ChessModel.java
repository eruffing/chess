/***********************************************************************
 * A class to model a game of chess. Handles the game play.
 * 
 * @author Ethan Ruffing
 * @version 1.2.2
 **********************************************************************/
package chess;

import W14Project3.IChessModel;
import W14Project3.IChessPiece;
import W14Project3.Move;
import W14Project3.Player;

public class ChessModel implements IChessModel {
	private IChessPiece[][] board;
	private Player player;
	private Player winner;

	/** The number of black pieces captured by white. */
	private int capturedByWhite;

	/** The number of white pieces captured by black. */
	private int capturedByBlack;

	/**
	 * A boolean used to prevent further action if game over has been
	 * determined.
	 */
	private boolean gameOver;

	/*******************************************************************
	 * Default constructor. Creates a standard eight-by-eight chess
	 * game.
	 ******************************************************************/
	public ChessModel() {
		// Initialize board
		board = new IChessPiece[8][8];

		// Initialize captured counts to zero
		capturedByWhite = 0;
		capturedByBlack = 0;

		// Set the first player to white
		player = Player.WHITE;

		// Initialize gameOver to false
		gameOver = false;

		// Set up white pieces
		board[0][0] = new Rook(Player.WHITE);
		board[0][1] = new Knight(Player.WHITE);
		board[0][2] = new Bishop(Player.WHITE);
		board[0][3] = new Queen(Player.WHITE);
		board[0][4] = new King(Player.WHITE);
		board[0][5] = new Bishop(Player.WHITE);
		board[0][6] = new Knight(Player.WHITE);
		board[0][7] = new Rook(Player.WHITE);
		for (int col = 0; col < board[1].length; col++) {
			board[1][col] = new Pawn(Player.WHITE);
		}

		// Clear middle spaces
		for (int row = 2; row < 6; row++) {
			for (int col = 0; col < board[2].length; col++) {
				board[row][col] = null;
			}
		}

		// Set up black pieces
		board[7][0] = new Rook(Player.BLACK);
		board[7][1] = new Knight(Player.BLACK);
		board[7][2] = new Bishop(Player.BLACK);
		board[7][3] = new Queen(Player.BLACK);
		board[7][4] = new King(Player.BLACK);
		board[7][5] = new Bishop(Player.BLACK);
		board[7][6] = new Knight(Player.BLACK);
		board[7][7] = new Rook(Player.BLACK);
		for (int col = 0; col < board[6].length; col++) {
			board[6][col] = new Pawn(Player.BLACK);
		}

		winner = null;
	}

	/*******************************************************************
	 * Method to determine whether the game is over (e.g., checkmate or
	 * stalemate has been reached).
	 * 
	 * @return Whether the game is over
	 ******************************************************************/
	public boolean isComplete() {
		if (this.gameOver) {
			return true;
		}

		// If white's king is in check, see if it is checkmate
		if (inCheck(Player.WHITE)) {
			// Determine location of white's king
			int rKing = -1;
			int cKing = -1;
			for (int row = 0; row < board.length; row++) {
				for (int col = 0; col < board[0].length; col++) {
					if (board[row][col] != null) {
						if (board[row][col].type().equals("King")
								&& board[row][col].player() 
								== Player.WHITE) {
							rKing = row;
							cKing = col;
						}
					}
				}
			}

			// If the king wasn't found, it has been captured, and,
			// therefore, the game is over
			if (rKing == -1 && cKing == -1) {
				gameOver = true;
				return true;
			}

			// Determine if white king can move to another location
			boolean whiteCanMove = false;
			for (int row = 0; row < board.length; row++) {
				for (int col = 0; col < board.length; col++) {
					if (board[rKing][cKing].isValidMove(new Move(rKing,
							cKing, row, col), board)) {
						whiteCanMove = true;
					}
				}
			}

			// Determine the location of attacking piece
			int rAttk = -1;
			int cAttk = -1;
			for (int row = 0; row < board.length; row++) {
				for (int col = 0; col < board[0].length; col++) {
					if (board[row][col] != null) {
						if (board[row][col].isValidMove(new Move(row,
								col, rKing, cKing), board)) {
							rAttk = row;
							cAttk = col;
						}
					}
				}
			}

			// Determine if attacking piece can be stopped
			boolean canBeStopped = false;
			if (board[rAttk][cAttk].type().equals("Knight")) {
				canBeStopped = false;
			} else {
				for (int row = 0; row < board.length; row++) {
					for (int col = 0; col < board[0].length; col++) {
						for (int r1 = 0; r1 < board.length; r1++) {
							for (int c1 = 0; c1 < board[0].length;
									c1++) {
								if (board[row][col] != null) {
									if (board[row][col].isValidMove(
											new Move(row, col, r1, c1),
											board)) {
										canBeStopped = true;
									}
								}
							}
						}
					}
				}
			}
			for (int row = 0; row < board.length; row++) {
				for (int col = 0; col < board[0].length; col++) {
					if (board[row][col] != null) {
						if (board[row][col].isValidMove(new Move(row,
								col, rAttk, cAttk), board)) {
							canBeStopped = true;
						}
					}
				}
			}

			// Return true if white is in checkmate
			if (!whiteCanMove && !canBeStopped) {
				gameOver = true;
			}
		}

		// If black's king is in check, see if it is checkmate
		if (inCheck(Player.BLACK)) {
			// Determine location of black's king
			int rKing = -1;
			int cKing = -1;
			for (int row = 0; row < board.length; row++) {
				for (int col = 0; col < board[0].length; col++) {
					if (board[row][col] != null) {
						if (board[row][col].type().equals("King")
								&& board[row][col].player() 
								== Player.BLACK) {
							rKing = row;
							cKing = col;
						}
					}
				}
			}

			// If the king wasn't found, it has been captured, and,
			// therefore, the game is over
			if (rKing == -1 && cKing == -1) {
				gameOver = true;
				return true;
			}

			// Determine if white king can move to another location
			boolean blackCanMove = false;
			for (int row = 0; row < board.length; row++) {
				for (int col = 0; col < board.length; col++) {
					if (board[row][col] != null) {
						if (board[rKing][cKing].isValidMove(new Move(
								rKing, cKing, row, col), board)) {
							blackCanMove = true;
						}
					}
				}
			}

			// Determine the location of attacking piece
			int rAttk = -1;
			int cAttk = -1;
			for (int row = 0; row < board.length; row++) {
				for (int col = 0; col < board[0].length; col++) {
					if (board[row][col] != null) {
						if (board[row][col].isValidMove(new Move(row,
								col, rKing, cKing), board)) {
							rAttk = row;
							cAttk = col;
						}
					}
				}
			}

			// Determine if attacking piece can be stopped
			boolean canBeStopped = false;
			if (board[rAttk][cAttk].type().equals("Knight")) {
				canBeStopped = false;
			} else {
				for (int row = 0; row < board.length; row++) {
					for (int col = 0; col < board[0].length; col++) {
						for (int r1 = 0; r1 < board.length; r1++) {
							for (int c1 = 0; c1 < board[0].length;
									c1++) {
								if (board[row][col] != null) {
									if (board[row][col].isValidMove(
											new Move(row, col, r1, c1),
											board)) {
										canBeStopped = true;
									}
								}
							}
						}
					}
				}
			}
			for (int row = 0; row < board.length; row++) {
				for (int col = 0; col < board[0].length; col++) {
					if (board[row][col] != null) {
						if (board[row][col].isValidMove(new Move(row,
								col, rAttk, cAttk), board)) {
							canBeStopped = true;
						}
					}
				}
			}

			// Return true if white is in checkmate
			if (!blackCanMove && !canBeStopped) {
				gameOver = true;
			}
		}

		// Determine if there are any pieces left besides the two kings
		// to see if game is stalemate
		boolean anyLeft = false;
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[0].length; col++) {
				if (board[row][col] != null) {
					if (!board[row][col].type().equals("King")) {
						anyLeft = true;
					}
				}
			}
		}
		if (!anyLeft) {
			gameOver = true;
		}

		return gameOver;
	}

	/*******************************************************************
	 * Determines whether a given move is valid by passing the necessary
	 * information to the <code>isValidMove</code> method of the piece
	 * performing the move.
	 * 
	 * @param move The move to test
	 * @return Whether the specified move is valid
	 ******************************************************************/
	public boolean isValidMove(Move move) {
		return this.isValidMove(move, this.player);
	}

	/*******************************************************************
	 * Determines whether a given move is valid by passing the necessary
	 * information to the <code>isValidMove</code> method of the piece
	 * performing the move.
	 * 
	 * @param move The move to test
	 * @param player The player that would make the move
	 * @return Whether the specified move is valid
	 ******************************************************************/
	public boolean isValidMove(Move move, Player player) {
		if (board[move.fromRow][move.fromColumn] == null) {
			return false;
		} else if (board[move.fromRow][move.fromColumn].player() 
				!= player) {
			return false;
		} else {
			return board[move.fromRow][move.fromColumn].isValidMove(
					move, board);
		}
	}

	/*******************************************************************
	 * Performs the indicated move. Note that this method assumes that
	 * the move has already been checked for validity using
	 * <code>isValidMove</code>.
	 * 
	 * @param move The move to perform
	 ******************************************************************/
	public void move(Move move) {
		// If capturing a piece, update the appropriate capture count
		if (board[move.toRow][move.toColumn] != null) {
			if (board[move.toRow][move.toColumn].player() 
					== Player.WHITE) {
				capturedByBlack++;
			} else {
				capturedByWhite++;
			}
			if (board[move.toRow][move.toColumn].type()
					.equals("King")) {
				winner = board[move.fromRow][move.fromColumn].player();
				gameOver = true;
			}
		}

		// Perform the move
		board[move.toRow][move.toColumn] =
				board[move.fromRow][move.fromColumn];
		board[move.fromRow][move.fromColumn] = null;

		// Switch players
		player = player.next();
	}

	/*******************************************************************
	 * Method to determine whether the given player is in check.
	 * 
	 * @param p The player who might be in check
	 * @return Whether the given player is in check
	 ******************************************************************/
	public boolean inCheck(Player p) {
		boolean check = false;

		// Determine location of player's king
		int rKing = -1;
		int cKing = -1;
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[0].length; col++) {
				if (board[row][col] != null) {
					if (board[row][col].type().equals("King")
							&& board[row][col].player() == p) {
						rKing = row;
						cKing = col;
					}
				}
			}
		}

		// Determine if any pieces are able to attack the king
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[0].length; col++) {
				Player p1 =
						p == Player.BLACK ? Player.WHITE : Player.BLACK;
				if (isValidMove(new Move(row, col, rKing, cKing), p1)) {
					check = true;
				}
			}
		}

		return check;
	}

	/*******************************************************************
	 * Service method to determine which player's turn it is.
	 * 
	 * @return The current player
	 ******************************************************************/
	public Player currentPlayer() {
		return player;
	}

	/*******************************************************************
	 * Service method to determine the number of rows in the board.
	 * 
	 * @return The number of rows in the game board
	 ******************************************************************/
	public int numRows() {
		return board.length;
	}

	/*******************************************************************
	 * Service method to determine the number of columns in the board.
	 * 
	 * @return The number of columns in the game board
	 ******************************************************************/
	public int numColumns() {
		return board[0].length;
	}

	/*******************************************************************
	 * Service method to access a piece at a given location int the game
	 * board.
	 * 
	 * @return The piece at the specified location in the game board
	 ******************************************************************/
	public IChessPiece pieceAt(int row, int col) {
		if (board[row][col] != null)
			return board[row][col];
		else
			return null;
	}

	/*******************************************************************
	 * Service method to access the number of pieces captured by white.
	 * 
	 * @return The number of black pieces captured by white
	 ******************************************************************/
	public int getCapturedByWhite() {
		return capturedByWhite;
	}

	/*******************************************************************
	 * Service method to access the number of pieces captured by black.
	 * 
	 * @return The number of white pieces captured by black
	 ******************************************************************/
	public int getCapturedByBlack() {
		return capturedByBlack;
	}

	/**
	 * Service method to get the winner at the end of the game.
	 * 
	 * @return The winner, or null
	 */
	public Player getWinner() {
		return winner;
	}
}
