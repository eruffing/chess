/***********************************************************************
 * This class contains <code>JUnit</code> tests to test the
 * <code>ChessModel</code> class.
 * 
 * @author Ethan Ruffing
 * @version 1.2.2
 **********************************************************************/
package chess;

import static org.junit.Assert.*;

import org.junit.Test;

import W14Project3.Move;
import W14Project3.Player;

public class TestChessModel {

	/*******************************************************************
	 * Test that the constructor properly initializes variables.
	 ******************************************************************/
	@Test
	public void testConstructor() {
		ChessModel model = new ChessModel();
		assertEquals("Expected 8 rows, got " + model.numRows(), 8,
				model.numRows());
		assertEquals("Expected 8 columns, got " + model.numColumns(),
				8, model.numColumns());
		assertEquals("Expected WHITE, got " + model.currentPlayer(),
				Player.WHITE, model.currentPlayer());
		assertEquals(
				"Expected 0 captured by white, got "
						+ model.getCapturedByWhite(), 0,
				model.getCapturedByWhite());
		assertEquals(
				"Expected 0 captured by black, got "
						+ model.getCapturedByBlack(), 0,
				model.getCapturedByBlack());
		for (int row = 2; row < 6; row++) {
			for (int col = 0; col < 8; col++) {
				assertNull("Expected null in [" + row + "][" + col
						+ "], found a game piece.",
						model.pieceAt(row, col));
			}
		}
	}

	/*******************************************************************
	 * Test that the <code>move</code> function works properly.
	 ******************************************************************/
	@Test
	public void testMove() {
		ChessModel model = new ChessModel();
		Player p = model.pieceAt(1, 5).player();
		String t = model.pieceAt(1, 5).type();
		model.move(new Move(1, 5, 3, 4));
		assertEquals("Expeced player to be " + p.toString() + ", got "
				+ model.pieceAt(3, 4).player().toString(), p, model
				.pieceAt(3, 4).player());
		assertTrue("Expected type to be " + t + ", got "
				+ model.pieceAt(3, 4).type(),
				t.equals(model.pieceAt(3, 4).type()));

	}

	/*******************************************************************
	 * Test that the <code>inCheck</code> function works properly for
	 * the white player.
	 ******************************************************************/
	@Test
	public void testWhiteInCheck() {
		ChessModel model = new ChessModel();
		model.move(new Move(7, 3, 0, 3));
		assertTrue(
				"Expected white to be in check, got white is not in "
				+ "check",
				model.inCheck(Player.WHITE));
	}

	/*******************************************************************
	 * Test that the <code>inCheck</code> function works properly for
	 * the black player.
	 ******************************************************************/
	@Test
	public void testBlackInCheck() {
		ChessModel model = new ChessModel();
		model.move(new Move(0, 3, 7, 3));
		assertTrue(
				"Expected black to be in check, got black is not in "
				+ "check",
				model.inCheck(Player.BLACK));
	}
}
