/***********************************************************************
 * A pawn class. This class extends the <code>ChessPiece</code> class,
 * implementing actions specific to the pawn piece.
 * 
 * @author Ethan Ruffing
 * @version 1.2.2
 **********************************************************************/
package chess;

import W14Project3.IChessPiece;
import W14Project3.Move;
import W14Project3.Player;

public class Pawn extends ChessPiece {

	/*******************************************************************
	 * Default constructor. Creates a new pawn with the specified player
	 * as its owner.
	 * 
	 * @param player The player the bishop will play for
	 ******************************************************************/
	public Pawn(Player player) {
		super(player);
	}

	/*******************************************************************
	 * A string representation of the type of chess piece.
	 * 
	 * @return "Pawn"
	 ******************************************************************/
	public String type() {
		return "Pawn";
	}

	/*******************************************************************
	 * Determines whether a given move is valid for a pawn. Allows only
	 * one-square forward motion, with the exception of the first move.
	 * Also allows a one-square diagonal attack. Has additional limits
	 * set by <code>isValidMove</code> in <code>ChessPiece</code> .
	 * 
	 * @param move The move to test
	 * @param board The board the move would be performed on
	 ******************************************************************/
	public boolean isValidMove(Move move, IChessPiece[][] board) {
		boolean valid = super.isValidMove(move, board);
		// TODO Complete this

		// Skip further checks if not necessary
		if (!valid) {
			return false;
		}

		// Only move one row forwards unless it is the first move (also
		// ensures forward motion)
		if (player() == Player.WHITE) {
			if ((move.toRow - move.fromRow != 1)
					&& !(move.fromRow == 1 && move.toRow == 3)) {
				valid = false;
			}
			// Cannot skip over another piece for 2-square first move
			if (move.fromRow == 1 && move.toRow == 3
					&& board[2][move.fromColumn] != null) {
				valid = false;
			}
		} else {
			if ((move.toRow - move.fromRow != -1)
					&& !(move.fromRow == 6 && move.toRow == 4)) {
				valid = false;
			}
			// Cannot skip over another piece for 2-square first move
			if (move.fromRow == 6 && move.toRow == 4
					&& board[5][move.fromColumn] != null) {
				valid = false;
			}
		}

		// Only move horizontally if it is part of a one-square diagonal
		// attack
		if (move.toColumn != move.fromColumn) {
			if (move.fromColumn - move.toColumn == 1
					|| move.fromColumn - move.toColumn == -1) {
				if (board[move.toRow][move.toColumn] == null) {
					valid = false;
				} else if (Math.abs(move.toRow - move.fromRow) != 1) {
					valid = false;
				} else if (board[move.toRow][move.toColumn].player() ==
															player()) {
					valid = false;
				}
			} else {
				valid = false;
			}
		}

		// Cannot attack vertically
		if (move.toColumn == move.fromColumn) {
			if (board[move.toRow][move.toColumn] != null) {
				valid = false;
			}
		}

		return valid;
	}
}
