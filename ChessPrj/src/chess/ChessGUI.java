/***********************************************************************
 * This class constructs and shows a <code>JFrame</code> that contains a
 * <code>ChessPanel</code>. This is the starting point for the chess
 * game, and contains the <code>main</code> method for the program.
 * 
 * @author Ethan Ruffing
 * @version 1.2.2
 **********************************************************************/
package chess;

import javax.swing.JFrame;

public class ChessGUI {

	/*******************************************************************
	 * Main method. Constructs and opens a <code>JFrame</code> that
	 * contains a <code>ChessPanel</code> and its <code>JMenuBar</code>.
	 * 
	 * @param args Command-line arguments. (These are ignored).
	 ******************************************************************/
	public static void main(String[] args) {
		JFrame frame = new JFrame("Chess Game");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		ChessPanel panel = new ChessPanel();
		frame.getContentPane().add(panel);
		frame.setJMenuBar(panel.getMenuBar());

		frame.pack();
		frame.setVisible(true);
	}

}
