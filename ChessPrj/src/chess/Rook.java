/***********************************************************************
 * A rook class. This class extends the <code>ChessPiece</code> class,
 * implementing actions specific to the rook piece.
 * 
 * @author Ethan Ruffing
 * @version 1.2.2
 **********************************************************************/
package chess;

import W14Project3.IChessPiece;
import W14Project3.Move;
import W14Project3.Player;

public class Rook extends ChessPiece {

	/*******************************************************************
	 * Default constructor. Creates a new rook with the specified player
	 * as its owner.
	 * 
	 * @param player The player the bishop will play for
	 ******************************************************************/
	public Rook(Player player) {
		super(player);
	}

	/*******************************************************************
	 * A string representation of the type of chess piece.
	 * 
	 * @return "Rook"
	 ******************************************************************/
	public String type() {
		return "Rook";
	}

	/*******************************************************************
	 * Determines whether a given move is valid for a rook. Allows only
	 * straight moves of any length and does not allow jumps over other
	 * pieces, with additional limits set by <code>isValidMove</code> in
	 * <code>ChessPiece</code>.
	 * 
	 * @param move The move to test
	 * @param board The board the move would be performed on
	 ******************************************************************/
	public boolean isValidMove(Move move, IChessPiece[][] board) {
		boolean valid = super.isValidMove(move, board);
		// TODO Complete this

		// No diagonal moves
		if (move.toRow != move.fromRow
				&& move.toColumn != move.fromColumn) {
			valid = false;
		}

		// Vertical moves allowed, but not across other pieces
		if (move.toRow != move.fromRow
				&& move.toColumn == move.fromColumn) {
			for (int i = move.fromRow + 1; i < move.toRow; i++) {
				if (board[i][move.fromColumn] != null) {
					valid = false;
				}
			}
			for (int i = move.fromRow - 1; i > move.toRow; i--) {
				if (board[i][move.fromColumn] != null) {
					valid = false;
				}
			}
		}

		// Horizontal moves allowed, but not across other pieces
		if (move.toRow == move.fromRow
				&& move.toColumn != move.fromColumn) {
			for (int i = move.fromColumn + 1; i < move.toColumn; i++) {
				if (board[move.fromRow][i] != null) {
					valid = false;
				}
			}
			for (int i = move.fromColumn - 1; i > move.toColumn; i--) {
				if (board[move.fromRow][i] != null) {
					valid = false;
				}
			}
		}

		return valid;
	}

}
